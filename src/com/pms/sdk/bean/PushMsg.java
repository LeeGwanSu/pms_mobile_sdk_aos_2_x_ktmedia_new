package com.pms.sdk.bean;

import android.os.Bundle;

import com.pms.sdk.IPMSConsts;

public class PushMsg implements IPMSConsts {

	public String msgId;
	public String notiTitle;
	public String notiMsg;
	public String notiImg;
	public String message;
	public String sound;
	public String msgType;
	public String ocmp;
	public String data;

	public PushMsg(Bundle extras) {
		msgId = extras.getString(KEY_MSG_ID);
		notiTitle = extras.getString(KEY_NOTI_TITLE);
		notiMsg = extras.getString(KEY_NOTI_MSG);
		notiImg = extras.getString(KEY_NOTI_IMG);
		message = extras.getString(KEY_MSG);
		sound = extras.getString(KEY_SOUND);
		msgType = extras.getString(KEY_MSG_TYPE);
		data = extras.getString(KEY_DATA);
		ocmp = extras.getString(KEY_OCMP);
	}

	@Override
	public String toString () {
		return String.format("onMessage:msgId=%s, notiTitle=%s, notiMsg=%s, notiImg=%s, message=%s, sound=%s, msgType=%s, data=%s, ocmp=%s", msgId,
				notiTitle, notiMsg, notiImg, message, sound, msgType, data, ocmp);
	}
}
