package com.pms.sdk.api.request;

import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;

public class SetConfig extends BaseRequest {

    public SetConfig(Context context) {
        super(context);
    }

    /**
     * get param
     *
     * @param msgFlag
     * @param notiFlag
     * @return
     */
    public JSONObject getParam(String msgFlag, String notiFlag, String contentnotiFlag, String eventnotiFlag, String custId) {
        JSONObject jobj;

        try {
            jobj = new JSONObject();
            jobj.put("msgFlag", msgFlag);
            jobj.put("notiFlag", notiFlag);
            jobj.put("contentsNotiFlag", contentnotiFlag);
            jobj.put("eventNotiFlag", eventnotiFlag);
            jobj.put("custId", custId);
            if (StringUtil.isEmpty(PMSUtil.getOtnId(mContext))) {
                jobj.put("otnId", "");
            } else {
                jobj.put("otnId", PMSUtil.getOtnId(mContext));
            }

            return jobj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * request
     *
     * @param apiCallback
     */
    public void request(String msgFlag, String notiFlag, String contentnotiFlag, String eventnotiFlag, String custId, final APICallback apiCallback) {
        try {
            apiManager.call(API_SET_CONFIG, getParam(msgFlag, notiFlag, contentnotiFlag, eventnotiFlag, custId), new APICallback() {
                @Override
                public void response(String code, JSONObject json) {
                    if (CODE_SUCCESS.equals(code)) {
                        requiredResultProc(json);
                    }
                    if (apiCallback != null) {
                        apiCallback.response(code, json);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * required result proccess
     *
     * @param json
     */
    private boolean requiredResultProc(JSONObject json) {
        try {
            mPrefs.putString(PREF_MSG_FLAG, json.getString("msgFlag"));
            mPrefs.putString(PREF_NOTI_FLAG, json.getString("notiFlag"));
            mPrefs.putString(PREF_CONTENT_NOTI_FLAG, json.getString("contentsNotiFlag"));
            mPrefs.putString(PREF_EVENT_NOTI_FLAG, json.getString("eventNotiFlag"));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
