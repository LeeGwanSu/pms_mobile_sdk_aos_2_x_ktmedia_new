package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;

public class GetConfig extends BaseRequest {

	public GetConfig(Context context) {
		super(context);
	}

	/**
	 * request
	 * 
	 * @param userid
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {
		try {
			apiManager.call(API_GET_CONFIG, new JSONObject(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			mPrefs.putString(PREF_MSG_FLAG, json.getString("msgFlag"));
			mPrefs.putString(PREF_NOTI_FLAG, json.getString("notiFlag"));
			mPrefs.putString(PREF_CONTENT_NOTI_FLAG, json.getString("contentsNotiFlag"));
			mPrefs.putString(PREF_EVENT_NOTI_FLAG, json.getString("eventNotiFlag"));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
