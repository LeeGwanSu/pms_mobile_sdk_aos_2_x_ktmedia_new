package com.pms.sdk;

import java.io.Serializable;
import java.util.Stack;

import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.PushReceiver;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.Badge;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class) kt_media
 * @version [2014.01.13 09:31] MQTT Client 안정화 & Connect Check 부분 수정함. <br>
 *          [2014.02.07 17:12] Collect 부분 수정함. <br>
 *          [2014.02.11 10:18] CollectLog getParam 부분 수정함. <br>
 *          [2014.02.21 17:23] MQTT Client 안정화 및 Prvate 서버 Protocol 적용 가능하게 수정함.<br>
 *          [2014.02.25 10:12] 안쓰는 method 삭제함.<br>
 *          [2014.03.03 10:36] 팝업 셋팅 default 설정 가능하게 변경.<br>
 *          [2014.03.05 15:31] MQTT Client 관련 Service 추가.<br>
 *          [2014.03.05 20:37] MQTT Client STOP 재시작하는 현상 수정.<br>
 *          [2014.03.13 10:47] MQTT Client keepAlive 수정함. sleep mode 삭제.<br>
 *          [2014.03.28 09:29] DisusePms.m API 추가함.<br>
 *          [2014.05.14 20:06] DisusePms.m API 삭제 & Popup 설정값 추가.<br>
 *          [2014.06.02 20:39] Popup WebView background 수정함. <br>
 *          [2014.06.05 18:24] 다른앱 실행시 팝업창 미 노출. <br>
 *          [2014.06.09 11:05] 팝업창에 대한 Flag 값 추가함. <br>
 *          [2014.06.11 17:32] 텍스트 푸쉬일때 toast 메세지로 전환할수 있도록 수정. <br>
 *          [2014.06.16 20:44] 다른앱 사용시 안뜨는 플래그 추가함. <br>
 *          [2014.06.24 09:10] ReadMSg UserMsgId로 가능하게 수정함. <br>
 *          [2014.07.02 15:28] 연속 발송시 1건씩 빠지는 문제 수정함. <br>
 *          [2014.07.03 12:28] MQTT Wake Lock 쪽 예외처리 추가함. <br>
 *          [2014.07.15 15:20] MQTT ping 관련 소스 추가함. 필요없는 소스 삭제. <br>
 *          [2014.07.24 10:41] MQTT 버그 수정 & pushImg 저장 가능하게 수정함.<br>
 *          [2014.07.25 10:30] 자동 ReadMsg 104 오류발생시 JSON 재구성해서 다시 호출하게 수정함.<br>
 *          [2014.08.01 15:01] Collect Log 버그 수정함. Push 받을 떄 null 체크하는 부분 계선.<br>
 *          [2014.08.04 13:29] MQTT 관련해서 category 사용자가 정의하게 수정함.<br>
 *          [2014.08.07 13:26] MQTT 안정화 & API Error Flag 수정. <br>
 *          [2014.08.21 10:41] MQTT 안정화 & API 호출시 Cache Clear 루틴 추가함.<br>
 *          [2014.09.01 13:00] Vollry lib 업데이트. <br>
 *          [2014.09.11 10:49] Notification Icon을 블러오는 위치를 Androidmanifest.xml 쪽으로 바꿈. <br>
 *          [2014.09.19 16:20] 적용되야하는 커스텀 사항 적용함. <br>
 *          [2014.10.13 10:27] 삭제 했던 authorize() 추가함. <br>
 *          [2014.10.21 14:46] Android OS 5.0에서 상태창 이미지 표시 안되는 버그 수정함. <br>
 *          [2014.10.23 14:29] 타앱 실행시 팝업노출 안하는 플래그값 디폴트로 수정함. <br>
 *          [2014.10.30 17:08] 타앱 실행시 팝업노출 안하는 플래그값 디폴트로 수정함. <- 재수정함. <br>
 *          [2015.03.13 16:32] 구글 정책에 따른 앱 실행중일떄만 팝업창 노출.<br>
 *          [2016.04.07 09:43] Push Payload값 추가함.<br>
 *          [2016.04.28 23:54] X509TrustManager 코드 삭제하고 대응 코드 삽입함.<br>
 *          [2016.06.30 11:20] Collect DB에서 삭제하고 호출 안하도록 수정함.<br>
 *          [2016.11.29 09:45] doze mode broadcast 패치1 적용 <br>
 *          [2016.12.14 13:21] Icon에 대한 버그 사항 수정함.<br>
 *          [2017.05.18 10:46] SetConfig API 수정 <br>
 *          [2017.10.27 11:49] * Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *          					* DB 중복 Open으로 인해 강제종료 문제 수정
 *          					* PushPopup 발생 시 강제 종료 문제 수정
 *          					* UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 *          					  UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함
*          	[2018.09.20 19:31] Android 8.0 대응, 잠금화면 체크 해제, 벨소리 볼륨적용, 잠금화면 체크, WRITE_STORAGE_PERMISSION 제거
 *          [2018.09.21 15:57] DB 버전 업그레이드
 *          [2018.12.11 16:57] MQTTBinder, FCM 적용
 *          [2019.01.08 12:58] MQTTStarter 제거, 분기별 서비스 적용
 *          [2019.01.18 16:49] Android 9.0에서 C2DM 토큰 안온다고해서 GCM IntentService로 긴급패치
 *          [2019.04.15 15:12] NO_TOKEN 대응 및 알림소리 원복기능 제거
 *
 */
public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;
	private static PMS instancePms;
	private static PMSPopup instancePmsPopup;
	private Context mContext;

	private final PMSDB mDB;
	private final Prefs mPrefs;

	private OnReceivePushListener onReceivePushListener;

	/**
	 * constructor
	 * 
	 * @param context
	 */
	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		this.mPrefs = new Prefs(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:201904151512");

		initOption(context);
	}

	/**
	 * initialize option
	 */
	private void initOption (Context context) {
		if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
			mPrefs.putString(PREF_NOTI_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
			mPrefs.putString(PREF_MSG_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_CONTENT_NOTI_FLAG))) {
			mPrefs.putString(PREF_CONTENT_NOTI_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_EVENT_NOTI_FLAG))) {
			mPrefs.putString(PREF_EVENT_NOTI_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
			mPrefs.putString(PREF_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			mPrefs.putString(PREF_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
			mPrefs.putString(PREF_ALERT_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
			mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
			mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
		}
		if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
			PMSUtil.setServerUrl(context, API_SERVER_URL);
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
			mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
		}
		if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
			mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
		}
		if (mPrefs.getBoolean("pref_setting_first") == false) {
			mPrefs.putBoolean(PREF_ISPOPUP_ACTIVITY, true);
			mPrefs.putBoolean("pref_setting_first", true);
		}
	}

	/**
	 * getInstance
	 *
	 * @param context
	 * @return
	 */
	public static PMS getInstance (Context context)
	{
		return getInstance(context, "");
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @param projectId
	 * @return
	 */
	public static PMS getInstance (final Context context, String projectId)
	{
		if (instancePms == null)
		{
			instancePms = new PMS(context);
		}

		instancePms.setmContext(context);
		if (!TextUtils.isEmpty(projectId))
		{
			PMSUtil.setGCMProjectId(context, projectId);
			CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		}

		String token = PMSUtil.getGCMToken(context);
		if (TextUtils.isEmpty(token) && NO_TOKEN.equals(token))
		{
			// get token
			FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>()
			{
				@Override
				public void onSuccess(InstanceIdResult instanceIdResult)
				{
					String newToken = instanceIdResult.getToken();
					PMSUtil.setGCMToken(context, newToken);
				}
			});
		}

		return instancePms;
	}

	public static PMSPopup getPopUpInstance () {
		return instancePmsPopup;
	}

	/**
	 * start mqtt service
	 */
	public void startMQTTService (Context context) {
		if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context))) {
			context.sendBroadcast(new Intent(context, RestartReceiver.class).setAction(ACTION_START));
		} else {
			context.stopService(new Intent(context, MQTTService.class));
		}
	}

	public void stopMQTTService (Context context) {
		context.stopService(new Intent(context, MQTTService.class));
	}

	/**
	 * clear
	 * 
	 * @return
	 */
	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
			instancePms.unregisterReceiver();
			instancePms = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	private void unregisterReceiver () {
		Badge.getInstance(mContext).unregisterReceiver();
	}

	// /**
	// * push token 세팅
	// * @param pushToken
	// */
	// public void setPushToken(String pushToken) {
	// CLog.i(TAG + "setPushToken");
	// CLog.d(TAG + "setPushToken:pushToken=" + pushToken);
	// PMSUtil.setGCMToken(mContext, pushToken);
	// }

	public void setPopupSetting (Boolean state, String title) {
		instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
	}

	/**
	 * set ServerUrl
	 * 
	 * @param serverUrl
	 */
	public void setServerUrl (String serverUrl) {
		CLog.i("setServerUrl");
		CLog.d("setServerUrl:serverUrl=" + serverUrl);
		PMSUtil.setServerUrl(mContext, serverUrl);
	}

	/**
	 * cust id 세팅
	 * 
	 * @param custId
	 */
	public void setCustId (String custId) {
		CLog.i("setCustId");
		CLog.d("setCustId:custId=" + custId);
		PMSUtil.setCustId(mContext, custId);
	}

	/**
	 * get cust id
	 * 
	 * @return
	 */
	public String getCustId () {
		CLog.i("getCustId");
		return PMSUtil.getCustId(mContext);
	}

	/**
	 * otn id 세팅
	 * 
	 * @param otnId
	 */
	public void setOtnId (String otnId) {
		PMSUtil.setOtnId(mContext, otnId);
	}

	/**
	 * get otn id
	 * 
	 * @return
	 */
	public String getOtnId () {
		return PMSUtil.getOtnId(mContext);
	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	public void setNotiOrPopup (Boolean isnotiorpopup) {
		PMSUtil.setNotiOrPopup(mContext, isnotiorpopup);
	}

	/**
	 * set inbox activity
	 * 
	 * @param inboxActivity
	 */
	public void setInboxActivity (String inboxActivity) {
		mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
	}

	/**
	 * set push popup activity
	 * 
	 * @param classPath
	 */
	public void setPushPopupActivity (String classPath) {
		mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
	}

	/**
	 * set pushPopup showing time
	 * 
	 * @param pushPopupShowingTime
	 */
	public void setPushPopupShowingTime (int pushPopupShowingTime) {
		mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
	}

	/**
	 * get msg flag
	 * 
	 * @return
	 */
	public String getMsgFlag () {
		return mPrefs.getString(PREF_MSG_FLAG);
	}

	/**
	 * get noti flag
	 * 
	 * @return
	 */
	public String getNotiFlag () {
		return mPrefs.getString(PREF_NOTI_FLAG);
	}

	/**
	 * get content noti flag
	 * 
	 * @return
	 */
	public String getContentNotiFlag () {
		return mPrefs.getString(PREF_CONTENT_NOTI_FLAG);
	}

	/**
	 * get event noti flag
	 * 
	 * @return
	 */
	public String getEventNotiFlag () {
		return mPrefs.getString(PREF_EVENT_NOTI_FLAG);
	}

	/**
	 * set noti icon
	 * 
	 * @param resId
	 */
	public void setNotiIcon (int resId) {
		mPrefs.putInt(PREF_NOTI_ICON, resId);
	}

	/**
	 * set large noti icon
	 * 
	 * @param resId
	 */
	public void setLargeNotiIcon (int resId) {
		mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
	}

	/**
	 * set noti cound
	 * 
	 * @param resId
	 */
	public void setNotiSound (int resId) {
		mPrefs.putInt(PREF_NOTI_SOUND, resId);
	}

	/**
	 * set noti receiver
	 * 
	 * @param intentAction
	 */
	public void setNotiReceiver (String intentAction) {
		mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
	}

	/**
	 * set ring mode
	 * 
	 * @param isRingMode
	 */
	public void setRingMode (boolean isRingMode) {
		mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
	}

	/**
	 * set vibe mode
	 * 
	 * @param isVibeMode
	 */
	public void setVibeMode (boolean isVibeMode) {
		mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	/**
	 * set popup noti
	 * 
	 * @param isShowPopup
	 */
	public void setPopupNoti (boolean isShowPopup) {
		mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	/**
	 * set screen wakeup
	 * 
	 * @param isScreenWakeup
	 */
	public void setScreenWakeup (boolean isScreenWakeup) {
		mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
	}

	public OnReceivePushListener getOnReceivePushListener () {
		return onReceivePushListener;
	}

	public void setOnReceivePushListener (OnReceivePushListener onReceivePushListener) {
		this.onReceivePushListener = onReceivePushListener;
	}

	/**
	 * pms device 인증
	 * 
	 * @return
	 */
	public void authorize () {
		CLog.i("authorize:start");
		// auth status setting
		new DeviceCert(mContext).request(null, new APICallback() {
			@Override
			public void response (String code, JSONObject json) {

				if (CODE_SUCCESS.equals(code)) {
					// success device cert
					CLog.i("DeviceCert:success device cert");
				} else {
					// fail device cert
					CLog.i("DeviceCert:fail device cert");
				}
			}
		});
	}

	/**
	 * set debugTag
	 * 
	 * @param tagName
	 */
	public void setDebugTAG (String tagName) {
		CLog.setTagName(tagName);
	}

	/**
	 * set debug mode
	 * 
	 * @param debugMode
	 */
	public void setDebugMode (boolean debugMode) {
		CLog.setDebugMode(mContext, debugMode);
	}

	public void bindBadge (Context c, int id) {
		Badge.getInstance(c).addBadge(c, id);
	}

	public void bindBadge (TextView badge) {
		Badge.getInstance(badge.getContext()).addBadge(badge);
	}

	/**
	 * show Message Box
	 * 
	 * @param c
	 */
	public void showMsgBox (Context c) {
		showMsgBox(c, null);
	}

	public void showMsgBox (Context c, Bundle extras) {
		CLog.i("showMsgBox");
		if (PhoneState.isAvailablePush()) {
			try {
				Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

				Intent i = new Intent(mContext, inboxActivity);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				if (extras != null) {
					i.putExtras(extras);
				}
				c.startActivity(i);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			// Intent i = new Intent(INBOX_ACTIVITY);
			// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// if (extras != null) {
			// i.putExtras(extras);
			// }
			// c.startActivity(i);
		}
	}

	public void closeMsgBox (Context c) {
		Intent i = new Intent(c, Badge.class).setAction(RECEIVER_CLOSE_INBOX);
		c.sendBroadcast(i);
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg
	 * 
	 *
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * select Msg
	 * 
	 *
	 * @return
	 */
	public Msg selectMsgWhereUserMsgId (String userMsgID) {
		return mDB.selectMsgWhereUserMsgId(userMsgID);
	}

	/**
	 * select query
	 * 
	 *
	 * @return
	 */
	public Cursor selectQuery (String sql) {
		return mDB.selectQuery(sql);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteUserMsgId (String userMsgId) {
		return mDB.deleteUserMsgId(userMsgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param MsgId
	 * @return
	 */
	public long deleteMsgId (String MsgId) {
		return mDB.deleteMsgId(MsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */

	public void setNotiReceiverClass (String className) {
		mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, className);
	}
	public void setPushReceiverClass (String className) {
		mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, className);
	}
}
